const filter = require("./filter.js");

describe("filter", () => {
  it("throws an error when called with no arguments", () => {
    expect(() => filter()).toThrow(
      "Please provide an array and predicate function."
    );
  });
  it("throws an error when called without a predicate", () => {
    expect(() => filter([])).toThrow(
      "Expected a predicate as the second argument"
    );
  });
  it("throws an error when called with an invalid predicate", () => {
    expect(() => filter([], "")).toThrow(
      "Expected the predicate to be a function"
    );
  });
  it("throws an error when the first argument is not an array", () => {
    expect(() => filter("1", () => {})).toThrow(
      "Expected an array as the first argument"
    );
  });
  it("returns an empty array when given an empty array and a predicate", () => {
    expect(filter([], () => true)).toEqual([]);
  });

  describe("Given an array with a single value", () => {
    it("returns an empty array when predicate is false", () => {
      expect(filter([""], () => false)).toEqual([]);
    });
    it("returns an array with the same value when the predicate is true", () => {
      expect(filter([1], (value) => value === 1)).toEqual([1]);
    });
  });

  describe("given an array with two items", () => {
    it("returns both items if the predicate returns true for both", () => {
      expect(filter([1, 2], () => true)).toEqual([1, 2]);
    });
    it("returns no items if predicate returns false for both", () => {
      expect(filter([1, 2], () => false)).toEqual([]);
    });
    it("returns an array with one item if the predicate returns true for one", () => {
      expect(filter([1, 2], (item) => item === 1)).toEqual([1]);
    });
  });
});
